import com.devcamp.Time;

public class App {
    public static void main(String[] args) throws Exception {
        
        Time time1 = new Time(3, 60, 50);
        Time time2 = new Time(17, 33, 45);

        time1.nextSecond();
        time2.previousSecond();

        System.out.println(time1);
        System.out.println(time2);
    }
}
